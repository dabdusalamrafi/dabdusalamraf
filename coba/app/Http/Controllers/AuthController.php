<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{  
    public function biodata(){
        return view('halaman.register');
    }
    public function kirim(Request $request){
        $namaDepan = $request['nama1'];
        $namaBelakang = $request['nama2'];
        return view('halaman.welcome',['namaDepan' => $namaDepan, 'namaBelakang'=>$namaBelakang]);
    }
    //
}
